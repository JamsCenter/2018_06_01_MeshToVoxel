﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoxelWall : MonoBehaviour {

    public VoxelPools m_pool;
    public float m_timbeBetween=0.1f;
    public float m_cubeSize = 4;


    int i = 0;
    int x, y, z;
    Color color = Color.white;
    Transform pixel;
    Vector3 scale = Vector3.one * 0.5f;
    Vector3 localPosition = Vector3.zero;

    int gg; 
    IEnumerator Start()
    {
        
        while (true) {
            gg++;
            for ( x = 0; x < m_cubeSize; x++)
            {
                for ( y = 0; y < m_cubeSize; y++)
                {
                    for ( z = 0; z < m_cubeSize; z++)
                    {
                        color.r = (i+ gg + 100) % 255 / 255f;
                        color.g = (i+ gg + 200) % 255 / 255f;
                        color.b = (i+ gg) % 255 / 255f;
                        localPosition.x = x % m_cubeSize;
                        localPosition.y = y % m_cubeSize;
                        localPosition.z = z % m_cubeSize;
                        m_pool.SetVoxel(i, color);
                        pixel = m_pool.GetVoxelTransform(i);
                        pixel.transform.position = transform.position+ localPosition;
                        pixel.transform.localScale = scale;
                        i++;
                        //yield return new WaitForSeconds(m_timbeBetween);
                        z %= 255;
                    }
                   // yield return new WaitForEndOfFrame();
                }
            }
            //m_cubeSize++;
            //i %= (int) (m_cubeSize * m_cubeSize * m_cubeSize);//m_pool.GetVoxelsCount();
            i %= m_pool.GetVoxelsCount();

            yield return new WaitForSeconds(m_timbeBetween);
        }
    }
    private float GetRandom(float min, float max)
    {
        return Random.Range(min, max);
    }
}
