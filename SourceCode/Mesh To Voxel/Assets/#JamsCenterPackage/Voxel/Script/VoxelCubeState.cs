﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoxelCubeState : MonoBehaviour {

    public int m_cubeSize=16;
    [Header("Debug")]
    public VoxelCubeStateData m_cubeState;
	// Use this for initialization
	void Awake () {
        m_cubeState = new VoxelCubeStateData(m_cubeSize);
	}
    public void SetVoxelTo(int x, int y, int z, Color color)
    {
        m_cubeState.SetColorTo(x, y, z, true, color);

    }
    public void SetVoxelTo(int x, int y, int z, bool isOn,Color color)
    {
        m_cubeState.SetColorTo(x, y, z, isOn, color);

    }
}
[System.Serializable]
public class VoxelCubeStateData {

    public string m_name="Default";
    public int m_size;
    public VoxelData[,,] m_colors;


    public VoxelCubeStateData(int size) {
        Reset(size, Color.white);
    }
    public void Reset(int size, Color color) {
        m_size = size;
        m_colors = new VoxelData[32, 32, 32];
    }

    public void SetColorTo(int x, int y, int z, bool isOn,Color color) {
        m_colors[x, y, z]= new VoxelData(isOn, color );
    }

}
[System.Serializable]
public struct VoxelData {

    public VoxelData(bool on, Color color)
    {
        m_on = on; m_color = color;
    }
    public bool m_on;
    public Color m_color ;
}
