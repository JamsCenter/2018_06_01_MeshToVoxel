﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;


//Remove Inside FAces:
//https://blender.stackexchange.com/questions/18916/how-to-remove-interior-faces-while-keeping-exterior-faces-untouched
public class BoxScanToVoxel : MonoBehaviour {

    [Header("Params")]
    public Transform m_start;
    public Transform m_end;
    public LayerMask m_mask;
    public int m_voxelCount = 100;
    public float raycastAngle = 36;


    public Transform m_container;
    public VoxelPools m_voxelPool;
    

    public VoxelDetected m_onCubeDetected;
    [System.Serializable]
    public class VoxelDetected : UnityEvent<int, int, int, Color> { }


    [Header("Debug  Params")]
    public float timeBetweenCube = 0.1f;

    [Header("Debug (Don't Touch)")]
    public float voxelSize;
    public float voxelRadius;
    public float m_poucentScanning = 0;

    public int createdCube;
    public int index;
    public int max;


    private Coroutine m_currentCouroutine;
    public bool IsScanning(){return m_currentCouroutine != null; }


    public void StartScanning()
    {
        m_currentCouroutine = StartCoroutine(CoroutineScan());
    }



    

    public IEnumerator CoroutineScan() {
       // float ratioMagnitudeEdge = Mathf.Sqrt(1 + Mathf.Sqrt(1 + 1));
        float m = Vector3.Distance(m_start.position, m_end.position);
        float a = Mathf.Abs(m_start.position.y - m_end.position.y);
        float b = Mathf.Abs(m_start.position.z - m_end.position.z);

        voxelSize = ((m * m) - Mathf.Pow(Mathf.Sqrt((a * a) + (b * b)), 2)) / m_voxelCount;
        voxelRadius = voxelSize * 0.5f;


         createdCube = 0;
         index=0;
         max = m_voxelCount * m_voxelCount * m_voxelCount;
        for (int x = 0; x < m_voxelCount; x++)
        {
            for (int z = 0; z < m_voxelCount; z++)
            {
                for (int y = 0; y < m_voxelCount; y++)
                {
                    index++;
                    Vector3 localPosition = Vector3.one * voxelRadius
                        + Vector3.right * voxelSize * x
                        + Vector3.up * voxelSize * y
                        + Vector3.forward * voxelSize * z;

                    Vector3 worldPosition = m_start.TransformPoint(localPosition);
                    Collider[] cols = Physics.OverlapSphere(worldPosition, voxelRadius, m_mask);
                    if (cols.Length > 0)
                    {
                        Transform obj = CreateBlock(createdCube, worldPosition, m_start.rotation, voxelSize);
                        if (obj != null)
                        {
                            obj.name += " (" + createdCube + ")";
                            createdCube++;

                            if(m_container!=null)
                               obj.transform.parent = m_container;
                        }


                        m_poucentScanning = (float)index / (float)max;
                        //yield return new WaitForEndOfFrame();
                        if(timeBetweenCube>0f)
                            yield return new WaitForSeconds(timeBetweenCube);

                    }
                }

            }
        }
        //CreatePrefab();
        m_currentCouroutine = null;
        AssetDatabase.Refresh();
        yield break;
    }

    //public string m_createdPrefabName="VoxelScan";
    //public GameObject m_createdPrefab;
    //private void CreatePrefab() { 
        

    //    string prefabPath = "Assets/Resources/"+ m_createdPrefabName + ".prefab";
    //    AssetDatabase.DeleteAsset(prefabPath);
    //    m_createdPrefab = PrefabUtility.CreatePrefab(prefabPath, m_container.gameObject);
    //}

    private Transform CreateBlock(int index, Vector3 position, Quaternion rotation, float size)
    {
        Color color = Color.black;
        if ( ! JC.Utility.Texture2D.RaycastForColorsAtPosition(position, size, out color, m_mask, raycastAngle))
            return null;

        Transform cube = m_voxelPool.GetVoxelTransform(index);
        m_voxelPool.SetVoxel(index, color);
        
        cube.transform.rotation = rotation;
        cube.transform.position = position;
        cube.transform.localScale = Vector3.one * size;
        Debug.DrawLine(position+ -Vector3.up / m_voxelCount, position + Vector3.up/m_voxelCount, Color.cyan, 100);
        return cube;
    }
  

    
  


   


    //#region UTILITY
    //#region SYSTEM WRITE
    //private Texture tempCreatedTextureInProject;
    //public Material CreateTextureAndMetarialInDat aPath(Texture2D texture, string name)
    //{
    //    // Create a simple material asset

    //    Directory.CreateDirectory(Application.dataPath + "/Resources/");
    //    string relativePath = "Resources/Text_" + name + ".png";
    //    string resource = "Text_" + name;
    //    string path = Application.dataPath + "/" + relativePath;
    //    Texture2D t = new Texture2D(texture.width, texture.height);
    //    t.SetPixels(texture.GetPixels());
    //    File.WriteAllBytes(path, t.EncodeToJPG());

    //    AssetDatabase.Refresh(ImportAssetOptions.DontDownloadFromCacheServer);

    //    tempCreatedTextureInProject = Resources.Load<Texture>(resource);
    //    Material material = new Material(Shader.Find("Standard"));
    //    AssetDatabase.CreateAsset(material, "Assets/Resources/Mat_" + name + ".mat");
    //    material = AssetDatabase.LoadAssetAtPath<Material>("Assets/Resources/Mat_" + name + ".mat");
    //    material.SetTexture("_MainTex", tempCreatedTextureInProject);
        
    //    return material;

    //}
    //#endregion
    //#endregion


}
