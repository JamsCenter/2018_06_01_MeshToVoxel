﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAround : MonoBehaviour {

    public float m_speed=90;
    public float m_range=20;
    public Vector3 m_direction;
    public Transform m_target;
    void Start () {
        while (m_direction== Vector3.zero)
        {

        m_direction = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f));
        }
        m_speed += Random.Range(-m_range, m_range);

    }
	
	void Update () {

        transform.RotateAround(m_target.position, m_direction, m_speed*Time.deltaTime);
        
	}
}
